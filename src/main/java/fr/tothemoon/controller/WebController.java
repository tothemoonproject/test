package fr.tothemoon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.tothemoon.entity.User;
import fr.tothemoon.service.UserService;

@RestController
public class WebController {

	@Autowired
	UserService userService;

	@RequestMapping("/save")
	public String process() {
		// save a single Customer
		userService.save(new User("meisha", "qwerty"));

		return "Done";
	}
	
	
	@RequestMapping("/findall")
	public String findAll(){
		String result = "";
		
		for(User user : userService.findAll(User.class)){
			result += user.getUsername() + "<br>";
			result += user.getPassword() + "<br>";
			result += "-------------------------------" + "<br>";
		}
		return result;
	}
	
	@RequestMapping("/findbyid")
	public String findById(@RequestParam("id") long id){
		String result = "";
		result = userService.findById(id).toString();
		return result;
	}
	
	@RequestMapping("/findbyusername")
	public String fetchDataByUsername(@RequestParam("username") String username){
		String result = "";
		
		User foundUser = userService.findByUsername(username);
		
		result += foundUser.toString() + "<br>"; 
		
		return result;
	}
	
}
