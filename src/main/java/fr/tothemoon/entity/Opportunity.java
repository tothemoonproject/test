package fr.tothemoon.entity;

import java.util.Date;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.dto.marketdata.Ticker;

public class Opportunity{
	
	private Date date;
	
	private Exchange exchange1;
	private Exchange exchange2;
	
	private String pair;
	
	private Ticker tickerExchange1;
	private Ticker tickerExchange2;
	
	public Opportunity() {
		
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Exchange getExchange1() {
		return exchange1;
	}

	public void setExchange1(Exchange exchange1) {
		this.exchange1 = exchange1;
	}

	public Exchange getExchange2() {
		return exchange2;
	}

	public void setExchange2(Exchange exchange2) {
		this.exchange2 = exchange2;
	}

	public String getPair() {
		return pair;
	}

	public void setPair(String pair) {
		this.pair = pair;
	}

	public Ticker getTickerExchange1() {
		return tickerExchange1;
	}

	public void setTickerExchange1(Ticker tickerExchange1) {
		this.tickerExchange1 = tickerExchange1;
	}

	public Ticker getTickerExchange2() {
		return tickerExchange2;
	}

	public void setTickerExchange2(Ticker tickerExchange2) {
		this.tickerExchange2 = tickerExchange2;
	}

	
	
}
