package fr.tothemoon.entity;

public class Strategy {
	
	private String id;
	private User user;
	
	private String name;
	private Long capital;
	private Boolean activate;
	
	private Double minPercentTreshold;
	
	private Double percentGainToCapital;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCapital() {
		return capital;
	}

	public void setCapital(Long capital) {
		this.capital = capital;
	}

	public Boolean getActivate() {
		return activate;
	}

	public void setActivate(Boolean activate) {
		this.activate = activate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Double getMinPercentTreshold() {
		return minPercentTreshold;
	}

	public void setMinPercentTreshold(Double minPercentTreshold) {
		this.minPercentTreshold = minPercentTreshold;
	}

	public Double getPercentGainToCapital() {
		return percentGainToCapital;
	}

	public void setPercentGainToCapital(Double percentGainToCapital) {
		this.percentGainToCapital = percentGainToCapital;
	}
	
}
