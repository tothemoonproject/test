package fr.tothemoon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
@Controller
public class ToTheMoonGuiApplication {

	private static Logger log = LoggerFactory.getLogger(ToTheMoonGuiApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ToTheMoonGuiApplication.class, args);
	}
	
	
	@GetMapping("/2fa_settings")
	public String twoFaSettings() {
		return "2fa_settings";
	}

	@GetMapping("/balance")
	public String balance() {
		return "balance";
	}
	
	@GetMapping("/home")
	public String home() {
		return "home";
	}

	@GetMapping("/home2")
	public String home2() {
		return "home2";
	}
	
	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/opportunityCheck")
	public String opportunityCheck() {
		return "opportunityCheck";
	}

	@GetMapping("/register")
	public String register() {
		return "register";
	}
	
	@GetMapping("/settings")
	public String settings() {
		return "settings";
	}

	@GetMapping("/strategies")
	public String strategies() {
		return "strategies";
	}
}
