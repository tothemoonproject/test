package fr.tothemoon.service;

import java.util.ArrayList;

import fr.tothemoon.entity.User;

public interface UserService {

	public void save(User object);

	public void delete(User object);
	
	public ArrayList<User> findAll(Class<User> type);
	
	public User findById(Long id);
	
	public User findByUsername(String username);
}
