package fr.tothemoon.service;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import fr.tothemoon.dao.UserDao;
import fr.tothemoon.entity.User;

@Service("UserService")
@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserServiceImpl implements UserService, Serializable {

	private static final long serialVersionUID = -9055888614332727534L;

	@Autowired
	UserDao<User> dao;
	
	@Override
	public void save(User user) {
		dao.save(user);
	}

	@Override
	public void delete(User user) {
		dao.delete(user);
	}

	@Override
	public ArrayList<User> findAll(Class<User> type) {
		return (ArrayList<User>) dao.findAll(type);
	}

	@Override
	public User findById(Long id) {
		return dao.get(id);
	}

	@Override
	public User findByUsername(String username) {
		return (User) dao.findByUsername(username);
	}

}
