package fr.tothemoon.viewModels;

import java.io.Serializable;

import org.apache.commons.lang3.math.NumberUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class settings_2faViewModel implements Serializable {

	private static final long serialVersionUID = 4043985689713513592L;

	private Window window;

	private String userName = "";
	private String password = "";
	private String code2fa;

	private String urlQrCode = "";
	private String secretKey = "";

	private GoogleAuthenticator gAuth;

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
		window = (Window) view;
	}

	@Command
	@SmartNotifyChange({ "secretKey", "urlQrCode" })
	public void activer_2fa() {

		gAuth = new GoogleAuthenticator();
		final GoogleAuthenticatorKey key = gAuth.createCredentials();

		System.out.println("key.getkey() : " + key.getKey());

		secretKey = key.getKey();

		String dataQrCode = "otpauth://totp/toTheMoon - connection:nicolas.brion9@orange.fr?secret=" + key.getKey()
				+ "&issuer=Meisha";

		String email = "nicolas.brion9@orange.fr";

		urlQrCode = "https://chart.googleapis.com/chart?cht=qr&chl=otpauth://totp/tothemoon - login (" + email
				+ ")?secret=" + key.getKey() + "&chs=150x150&choe=UTF-8&chld=L|2";

		System.out.println(urlQrCode);
		
		//todo save db
	}

	@Command
	@SmartNotifyChange({ "secretKey", "urlQrCode","code2fa" })
	public void desactiver_2fa() {
		secretKey = "";
		urlQrCode = "";
		code2fa = "";
		
		//todo delete db
	}

	@Command
	public void testCode2Fa() {
		if (NumberUtils.isDigits(code2fa)) {
			boolean isCodeValid = gAuth.authorize(secretKey, Integer.parseInt(code2fa));
			if (isCodeValid) {
				Clients.showNotification("2fa valide");
			} else {
				Clients.showNotification("2fa non valide");
			}
		}else{
			Clients.showNotification("2fa non valide");
		}
	}

	public String getUrlQrCode() {
		return urlQrCode;
	}

	public void setUrlQrCode(String urlQrCode) {
		this.urlQrCode = urlQrCode;
	}

	public String getCode2fa() {
		return code2fa;
	}

	public void setCode2fa(String code2fa) {
		this.code2fa = code2fa;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

}
