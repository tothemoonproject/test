package fr.tothemoon.viewModels;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.warrenstrange.googleauth.GoogleAuthenticator;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LoginViewModel {

	private String userName = "";
	private String password = "";
	private int code2fa = 0;

	private String urlQrCode = "";
	private String secretKey = "";

	private GoogleAuthenticator gAuth;
	private Window window;

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
		window = (Window) view;
	}

	@Command
//	@NotifyChange({ "secretKey", "urlQrCode" })
	public void login() {
//		gAuth = new GoogleAuthenticator();
//		final GoogleAuthenticatorKey key = gAuth.createCredentials();
//
//		System.out.println("key.getkey() : " + key.getKey());
//
//		secretKey = key.getKey();
//
//		String dataQrCode = "otpauth://totp/toTheMoon - connection:nicolas.brion9@orange.fr?secret=" + key.getKey()
//				+ "&issuer=Meisha";
//
//		String email = "nicolas.brion9@orange.fr";
//
//		urlQrCode = "https://chart.googleapis.com/chart?cht=qr&chl=otpauth://totp/tothemoon - login (" + email
//				+ ")?secret=" + key.getKey() + "&chs=150x150&choe=UTF-8&chld=L|2";
//		window.getFellow("qrCode").invalidate();
		
		Executions.getCurrent().sendRedirect("/home");
		
	}
	
	@Command
	public void redirectRegister() {
		Executions.getCurrent().sendRedirect("/register");
	}

	@Command
	public void testCode2Fa() {
		boolean isCodeValid = gAuth.authorize(secretKey,code2fa);
		if(isCodeValid) {
			Clients.showNotification("2fa valide");
		}else {
			Clients.showNotification("2fa non valide");
		}
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCode2fa() {
		return code2fa;
	}

	public void setCode2fa(int code2fa) {
		this.code2fa = code2fa;
	}

	public String getUrlQrCode() {
		return urlQrCode;
	}

	public void setUrlQrCode(String urlQrCode) {
		this.urlQrCode = urlQrCode;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

}
