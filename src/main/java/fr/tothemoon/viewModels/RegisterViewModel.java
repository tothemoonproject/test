package fr.tothemoon.viewModels;

import java.io.Serializable;

import org.zkoss.bind.annotation.Command;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

import fr.tothemoon.service.UserService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class RegisterViewModel implements Serializable {
	
	private static final long serialVersionUID = 22338532627062391L;
	
	private String email;
	private String userName;
	private String password;
	
	
	private UserService userService;
	@Command
	public void register() {
//		User user = userService;
	}
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
