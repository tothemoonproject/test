package fr.tothemoon.viewModels;

import java.io.Serializable;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zul.ListModelList;

import fr.tothemoon.entity.Strategy;


@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class StrategiesViewModel implements Serializable {
	
	private static final long serialVersionUID = -230256633861586362L;
	private ListModelList<Strategy> listStrategies;
	
	@Init
	public void init() {
		listStrategies = new ListModelList<Strategy>();
		
		simulateStrategies();
		
	}

	public void simulateStrategies() {
		Strategy strategy = new Strategy();
		strategy.setName("arbitrage OMG BITTREX - BITIFENEX");
		strategy.setMinPercentTreshold(0.1);
		strategy.setPercentGainToCapital(50D);
		
		listStrategies.add(strategy);
		
		Strategy strategy2 = new Strategy();
		strategy2.setName("arbitrage triangulaire BITTREX");
		strategy.setMinPercentTreshold(1D);
		strategy.setPercentGainToCapital(0D);
		
		listStrategies.add(strategy2);
	}
	
	public ListModelList<Strategy> getListStrategies() {
		return listStrategies;
	}

	public void setListStrategies(ListModelList<Strategy> listStrategies) {
		this.listStrategies = listStrategies;
	}
	
	

}
