package fr.tothemoon.viewModels;

import java.io.Serializable;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.VariableResolver;



@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
@Controller
public class HomeViewModel implements Serializable {

	private static final long serialVersionUID = -8717464574112850058L;

	private String currentPage = "";
	
	 @RequestMapping("/home")
	   public String home() {
		return "redirect:/home";
	   }

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
	}

	@Command
	@SmartNotifyChange("currentPage")
	public void ouvrirPage(@BindingParam("url") String url) {
		System.out.println(url);
		currentPage = url;
	}

	public String getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(String currentPage) {
		this.currentPage = currentPage;
	}
}
