package fr.tothemoon.viewModels;

import java.io.Serializable;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zul.Include;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class indexViewModel implements Serializable {

	private static final long serialVersionUID = -8717464574112850058L;
	private Include include;

	private String urlContentPage = "page.zul";

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {
		include = (Include) view;
	}

	@GlobalCommand
	@SmartNotifyChange("urlContentPage")
	public void ouvrirPage(@BindingParam("url") String url) {
		System.out.println(url);
		urlContentPage = url;
		include.invalidate();
	}

	public String getUrlContentPage() {
		return urlContentPage;
	}

	public void setUrlContentPage(String urlContentPage) {
		this.urlContentPage = urlContentPage;
	}

}
