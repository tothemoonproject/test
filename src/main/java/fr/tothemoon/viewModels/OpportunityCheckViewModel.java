package fr.tothemoon.viewModels;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;

import org.hibernate.Session;
import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.binance.BinanceExchange;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.service.account.AccountService;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.hibernate.HibernateUtil;
import org.zkoss.zul.ListModelList;

import fr.tothemoon.entity.Opportunity;
import fr.tothemoon.entity.User;
import fr.tothemoon.service.UserService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class OpportunityCheckViewModel implements Serializable {

	private static final long serialVersionUID = -3764558001206378916L;

	private ListModelList<Opportunity> listOpportunity = new ListModelList<Opportunity>();

	private Exchange exchange1;
	private AccountService accountService1;

	private Exchange exchange2;
	private AccountService accountService2;

	private ListModelList<CurrencyPair> listCommonPair;

	private CurrencyPair selectedCurrencyPair;
	
	@WireVariable
	private UserService UserService;

	public CurrencyPair getSelectedCurrencyPair() {
		return selectedCurrencyPair;
	}

	public void setSelectedCurrencyPair(CurrencyPair selectedCurrencyPair) {
		this.selectedCurrencyPair = selectedCurrencyPair;
	}

	private boolean autoCheckIsRunning = false;
	private Long autoCheckDelay = 5000L;

	public Long getAutoCheckDelay() {
		return autoCheckDelay;
	}

	public void setAutoCheckDelay(Long autoCheckDelay) {
		this.autoCheckDelay = autoCheckDelay;
	}

	public boolean isAutoCheckIsRunning() {
		return autoCheckIsRunning;
	}

	public void setAutoCheckIsRunning(boolean autoCheckIsRunning) {
		this.autoCheckIsRunning = autoCheckIsRunning;
	}

	@Init
	public void init(@ContextParam(ContextType.VIEW) Component view) {

		ExchangeSpecification exSpec1 = new BittrexExchange().getDefaultExchangeSpecification();
		exSpec1.setUserName("meisha");
		exSpec1.setApiKey("616c27ae91b94a0fad6e940de667ba7f");
		exSpec1.setSecretKey("2152c0d8bd09439f80eeb34fa9d889fd");
		exchange1 = ExchangeFactory.INSTANCE.createExchange(exSpec1);

		ExchangeSpecification exSpec2 = new BinanceExchange().getDefaultExchangeSpecification();
		exSpec1.setUserName("meisha");
		exSpec1.setApiKey("p61gTGQlBlLmP4UmezAuOHCCfktaqKMiLvWamZfXXqp6NYQNBg5NquacuPpit0ST");
		exSpec1.setSecretKey("invXaRkjyWZlJV64BWghX8k3PzwWqNxuw18MksHSJfyyunqvdTIyLxwKhCHW0Lza");
		exchange2 = ExchangeFactory.INSTANCE.createExchange(exSpec2);

		ArrayList<CurrencyPair> listPairExchange1 = (ArrayList<CurrencyPair>) exchange1.getExchangeSymbols();
		ArrayList<CurrencyPair> listPairExchange2 = (ArrayList<CurrencyPair>) exchange2.getExchangeSymbols();

		listCommonPair = new ListModelList<CurrencyPair>(listPairExchange1);
		listCommonPair.retainAll(listPairExchange2);
		listCommonPair.sort(new Comparator<CurrencyPair>() {
			public int compare(CurrencyPair o1, CurrencyPair o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});

	}

	@Command
	@SmartNotifyChange("listOpportunity")
	public void clearHistory() {
		listOpportunity.clear();
	}
	
//	@Command
//	public void testHibernate() {
//		System.out.println("testHibernate");
//		User user = new User();
//		user.setUsername("meisha");
//		user.setUserPassword("azerty");
//		
//		Configuration config = new Configuration();
//		config.addClass(User.class);
//		SessionFactory sessionFactory = config.buildSessionFactory();
//		Session session = sessionFactory.openSession();
//		
//		session.save(user);
//
//	}
	
	@Command
	public void testHibernate() {
		System.out.println("testHibernate");
		 Session session = HibernateUtil.getSessionFactory().openSession();
		  
	     session.beginTransaction();

	     User user = new User();
	     user.setUsername("test");
	     session.save(user);

	     session.getTransaction().commit();

	}
	

	@Command
	@SmartNotifyChange("autoCheckIsRunning")
	public void startAutoCheck() {
		autoCheckIsRunning = true;
		System.out.println("startAutoCheck");
	}

	@Command
	@SmartNotifyChange("autoCheckIsRunning")
	public void stopAutoCheck() {
		autoCheckIsRunning = false;
		System.out.println("stopAutoCheck");
	}

	@Command
	@SmartNotifyChange("listOpportunity")
	public void checkOpportunity() {

		if (selectedCurrencyPair != null) {

			if (Executions.getCurrent().getDesktop() == null) {
				System.out.println("desktop dead");
			} else {
				System.out.println("desktop alive");
			}

			try {
				Ticker ticker1 = exchange1.getMarketDataService().getTicker(selectedCurrencyPair);
				Ticker ticker2 = exchange2.getMarketDataService().getTicker(selectedCurrencyPair);

				Opportunity newOpportunity = new Opportunity();
				newOpportunity.setDate(new Date());
				newOpportunity.setExchange1(exchange1);
				newOpportunity.setTickerExchange1(ticker1);
				newOpportunity.setExchange2(exchange2);
				newOpportunity.setTickerExchange2(ticker2);
				newOpportunity.setPair(selectedCurrencyPair.toString());
				listOpportunity.add(newOpportunity);

				Opportunity newOpportunity2 = new Opportunity();
				newOpportunity2.setDate(new Date());
				newOpportunity2.setExchange1(exchange2);
				newOpportunity2.setTickerExchange1(ticker2);
				newOpportunity2.setExchange2(exchange1);
				newOpportunity2.setTickerExchange2(ticker1);
				newOpportunity2.setPair(selectedCurrencyPair.toString());
				listOpportunity.add(newOpportunity2);

				listOpportunity.sort(new Comparator<Opportunity>() {
					public int compare(Opportunity o1, Opportunity o2) {
						return o2.getDate().compareTo(o1.getDate());
					}
				});

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public ListModelList<Opportunity> getListOpportunity() {
		return listOpportunity;
	}

	public void setListOpportunity(ListModelList<Opportunity> listOpportunity) {
		this.listOpportunity = listOpportunity;
	}

	public ListModelList<CurrencyPair> getListCommonPair() {
		return listCommonPair;
	}

	public void setListCommonPair(ListModelList<CurrencyPair> listCommonPair) {
		this.listCommonPair = listCommonPair;
	}
}
