package fr.tothemoon.dao;

import java.util.List;

public interface GenericDao<T> {

	T get(Object id);

	void save(T object);

	void update(T object);

	void delete(T object);

	void detach(T object);

	void flush();

	T refresh(T object);

	List<T> findAll(Class<T> type);

	void setClazz(Class<T> clazzToSet);
}
