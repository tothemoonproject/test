package fr.tothemoon.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.tothemoon.entity.User;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao<User> {

	
	@Override
	public User findByUsername(String username) {

		String requete = "select * from User where username = '" + username + "'";
		
		List<User> userList = entityManager.createNativeQuery(requete, User.class).getResultList();
		
		if (!userList.isEmpty()) {
			return userList.get(0);
		}
		return null;
	}

}
