package fr.tothemoon.dao;

public interface UserDao<User> extends GenericDao<User> {
	
	User findByUsername(String username);

}
