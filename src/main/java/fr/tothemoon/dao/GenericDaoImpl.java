package fr.tothemoon.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public abstract class GenericDaoImpl<T extends Serializable> implements GenericDao<T> {

	@PersistenceContext
	protected EntityManager entityManager;
	protected String entity;
	protected Class<T> clazz;

	protected String JoinObjectName;

	public GenericDaoImpl() {
		super();
//		 this.clazz = getClazz();
	}

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public Class<T> getClazz() {
		return clazz;
	}

	@Transactional(readOnly = true)
	public T get(Object id) {
		return (T) entityManager.find(clazz, id);
	}

	@Transactional
	public void save(T object) {
		entityManager.persist(object);
	}

	@Transactional
	public void update(T object) {
		entityManager.merge(object);
	}

	@Transactional
	public void delete(T object) {
		object = entityManager.merge(object);
		entityManager.remove(object);
	}

	@Transactional
	public void detach(T object) {
		entityManager.detach(object);
	}

	@Transactional
	public void flush() {
		entityManager.flush();
	}

	@Transactional
	public T refresh(T object) {
		entityManager.refresh(object);
		return object;

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll(Class<T> type) {
		String requete = "select * from " + type.getAnnotation(Table.class).name();
		return entityManager.createNativeQuery(requete, type).getResultList();

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll(Boolean ASC, String val) {
		List<T> result = new ArrayList<T>();
		return result;

	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findByProperty(String prop, Object val) {
		List<T> result = new ArrayList<T>();
		return result;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findByProperty(HashMap<String, Object> map) {
		List<T> result = new ArrayList<T>();
		return result;
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}
